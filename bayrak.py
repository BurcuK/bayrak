import turtle 
import numpy

def draw_star(a):
	for i in range(5):
		a.forward(60)
		a.left(144)
		a.end_fill()

def draw_daire1(a):
	a.speed(100)
	for i in numpy.arange(276.1036):
		a.left(1)
		a.forward(1)

def draw_daire2(a):
	for i in numpy.arange(295.3438):
		a.left(1)
		a.forward(1)



wn=turtle.Screen()
wn.bgcolor("red")

alex=turtle.Turtle()
alex.color("white")

draw_star(alex)

alex.speed(100)

alex.left(180)
alex.color("red")
alex.forward(32)
alex.right(138.0518)
alex.forward(50)
alex.left(90)


alex.color("white")

draw_daire1(alex)

alex.left(99.6201)
alex.color("red")
alex.forward(60)
alex.right(115.3438)
alex.forward(66)
alex.left(90)

alex.color("white")

draw_daire2(alex)


turtle.mainloop()




